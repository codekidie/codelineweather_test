import Vue 	  	   from 'vue/dist/vue.js'
import Router 	   from 'vue-router';
import home      from './components/home.vue';
import weather      from './components/weather.vue';

Vue.use(Router);

const router = new Router({
  routes:[
    {
      path: '/',
  	  component: home   
    },
    {
      path: '/search/:keyword',
      component: home
    },
    {
      path: '/weather/:woeid',
      component: weather
    }
  ]
});

const app = new Vue ({
  el: '#app',
  router: router,

  mounted(){

    

  }

});